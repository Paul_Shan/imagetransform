# README #

A simple app to mosaic images.

### Main classes ###

* MainActivity  is used to select a image and to show the mosaic image result
* ImageBlockRepository uses RxJava to handle the parallel fetching data for each row of image.
* ApplicationTest is a simple unit test for fetching data

### Service Url ###
http://192.168.0.2:8765/color/32/32/$color
### Sample###
####Image####
![init.jpg](https://bitbucket.org/repo/xBnjR4/images/3108425281-init.jpg)
####Result####
![sample.png](https://bitbucket.org/repo/xBnjR4/images/1030456511-sample.png)