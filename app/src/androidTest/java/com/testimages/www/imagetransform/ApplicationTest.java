package com.testimages.www.imagetransform;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.test.ApplicationTestCase;

import com.squareup.picasso.Picasso;
import com.testimages.www.imagetransform.models.ImageBlock;
import com.testimages.www.imagetransform.models.ImageInfo;

import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    public void testFetchTiles()
    {
        Context context = getContext();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap =  BitmapFactory.decodeResource(context.getResources(), R.drawable.tiguan);
        ImageInfo initImageInfo = new ImageInfo(bitmap);
        Picasso picasso = Picasso.with(context);
        ImageBlockRepository imageBlockRepository = new ImageBlockRepository(initImageInfo, picasso);
        for(int i = 0; i < initImageInfo.getRowNumber();++i)
        {
            final int row = i;
            imageBlockRepository.getRowImageBlocks(i, Schedulers.immediate()).subscribe(new Subscriber<ImageBlock>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(ImageBlock imageBlock) {
                    assertEquals(imageBlock.getImageBlock().columnNumber,row);
                }
            });
        }




    }
}