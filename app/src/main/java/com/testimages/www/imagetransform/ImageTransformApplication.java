package com.testimages.www.imagetransform;

import android.app.Application;

import timber.log.Timber;

public class ImageTransformApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler((thread, ex) -> ex.printStackTrace());
        Timber.plant(new Timber.DebugTree());
    }
}
