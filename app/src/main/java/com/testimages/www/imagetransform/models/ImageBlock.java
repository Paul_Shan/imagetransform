package com.testimages.www.imagetransform.models;

import android.graphics.Bitmap;

import com.google.common.base.MoreObjects;

public class ImageBlock {
    final ImageBlockInfo imageBlock;
    final Bitmap subImage;

    public ImageBlock(ImageBlockInfo imageBlock, Bitmap subImage) {
        this.subImage = subImage;
        this.imageBlock = imageBlock;
    }

    public ImageBlockInfo getImageBlock() {
        return imageBlock;
    }

    public int getColumnNum() {
        return imageBlock.getColumnNumber();
    }

    public int getRowNum() {
        return imageBlock.getRowNumber();
    }

    public Bitmap getSubImage() {
        return subImage;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Rowumn", getRowNum())
                .add("Column", getColumnNum())
                .add("Height", subImage.getHeight())
                .add("Width", subImage.getWidth())
                .omitNullValues()
                .toString();
    }
}
