package com.testimages.www.imagetransform;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.squareup.picasso.Picasso;
import com.testimages.www.imagetransform.models.ImageBlock;
import com.testimages.www.imagetransform.models.ImageInfo;
import com.testimages.www.imagetransform.utility.ImageHelper;

import java.io.IOException;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    static final int PickImage = 1;
    ImageBlockRepository imageBlockRepository;
    Subject<Integer, Integer> rowSubject;
    private ImageView resultImageView;
    private Bitmap resultImage;
    private ImageInfo initImageInfo;
    Picasso picasso;
    final Stopwatch stopwatch = Stopwatch.createUnstarted();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        picasso = Picasso.with(this);
        resultImageView = (ImageView) findViewById(R.id.resultView);
        findViewById(R.id.selectImageButton).setOnClickListener(v -> {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, PickImage);
        });
    }

    private void renderRow(final int currentRow) {
        imageBlockRepository
                .getRowImageBlocks(currentRow)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ImageBlock>() {
                    @Override
                    public void onCompleted() {
                        resultImageView.setImageBitmap(resultImage);

                        if (currentRow < initImageInfo.getRowNumber() - 1) {
                            rowSubject.onNext(currentRow + 1);//Trigger rendering next row
                        } else {
                            rowSubject.onCompleted();
                            stopwatch.stop();
                            Timber.i("Running time " + stopwatch);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(ImageBlock imageBlock) {
                        Timber.i(imageBlock.toString());
                        int offsetX = initImageInfo.getPixelColOffset(imageBlock.getColumnNum());
                        int offsetY = initImageInfo.getPixelRowOffset(imageBlock.getRowNum());
                        Preconditions.checkState(imageBlock.getSubImage().getWidth() == initImageInfo.TileWidth);
                        Preconditions.checkState(imageBlock.getSubImage().getHeight() == initImageInfo.TileHeight);

                        ImageHelper.copyImageAndRecycleSource(imageBlock.getSubImage(), resultImage, offsetX, offsetY);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PickImage && resultCode == RESULT_OK) {
            stopwatch.reset();
            stopwatch.start();

            resultImageView.setImageBitmap(null);
            rowSubject = PublishSubject.create();
            Uri selectedImage = data.getData();
            Timber.i("imageUri" + selectedImage.getEncodedPath());
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                if (bitmap == null) {
                    return;
                }

                resultImage = Bitmap.createBitmap(bitmap.getWidth(),
                        bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                //resultImageView.setImageBitmap(resultImage);
                initImageInfo = new ImageInfo(bitmap);
                imageBlockRepository = new ImageBlockRepository(initImageInfo, picasso);

                Timber.i("Image: Height + " + initImageInfo.getHeight() + " Image Width " + initImageInfo.getWidth());
                if (initImageInfo.isImageValid()) {
                    rowSubject.subscribe(new Subscriber<Integer>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(Integer currentRow) {
                            renderRow(currentRow);
                        }
                    });
                    rowSubject.onNext(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
