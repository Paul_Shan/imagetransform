package com.testimages.www.imagetransform.utility;

import android.graphics.Bitmap;

import com.squareup.picasso.Picasso;
import com.testimages.www.imagetransform.models.ImageBlockInfo;
import com.testimages.www.imagetransform.models.ImageInfo;

import java.io.IOException;

import timber.log.Timber;

public class ImageHelper {
    public static void copyImageAndRecycleSource(Bitmap source, Bitmap destination, int offsetX, int offsetY) {

        int lengX = Math.min(source.getWidth(), destination.getWidth() - offsetX);
        int lengY = Math.min(source.getHeight(), destination.getHeight() - offsetY);

        if (lengX <= 0 || lengY <= 0) {
            return;
        }

        Timber.i("Copy Image source " + source.getWidth() + " X " + source.getHeight());
        Timber.i("Copy Image destination " + destination.getWidth() + " X " + destination.getHeight());
        Timber.i("Copy Image OffsetX " + offsetX + " OffsetY " + offsetY);
        for (int i = 0; i < source.getWidth(); ++i) {
            for (int j = 0; j < source.getHeight(); ++j) {
                int destX = i + offsetX;
                int destY = j + offsetY;
                if (destX >= destination.getWidth() || destY >= destination.getHeight()) {
                    break;
                }

                destination.setPixel(destX, destY, source.getPixel(i, j));
            }
        }

        source.recycle();
    }

    public static Bitmap loadTileFromService(Picasso picasso, String url) {
        Bitmap bitmap;
        try {
            bitmap = picasso.load(url).get();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return bitmap;
    }

    public static int getBlockAverageColor(ImageInfo imageInfo, ImageBlockInfo imageBlockInfo) {
        int startColumn = imageBlockInfo.getColumnNumber() * ImageInfo.TileWidth;
        int endColumn = Math.min(startColumn + ImageInfo.TileWidth, imageInfo.getWidth());
        int width = endColumn - startColumn;

        int startRow = imageBlockInfo.getRowNumber() * imageInfo.TileHeight;
        int endRow = Math.min(startRow + imageInfo.TileHeight, imageInfo.getHeight());
        int height = endRow - startRow;

        double avg = 0;
        int count = 1;

        for (int row = 0; row < height; ++row) {
            for (int column = 0; column < width; ++column) {
                int x = column + startColumn;
                int y = row + startRow;
                int v = imageInfo.getValue(x, y);
                avg += (v - avg) / count;
                ++count;
            }
        }
        Timber.i("width " + width + " height " + height + " startColumn " + startColumn + " endColumn " + endColumn + " startRow " + startRow + " endRow " + endRow);
        return (int) avg;
    }
}
