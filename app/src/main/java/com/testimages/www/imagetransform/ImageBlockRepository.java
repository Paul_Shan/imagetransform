package com.testimages.www.imagetransform;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;

import com.squareup.picasso.Picasso;
import com.testimages.www.imagetransform.models.ImageBlock;
import com.testimages.www.imagetransform.models.ImageBlockInfo;
import com.testimages.www.imagetransform.models.ImageInfo;
import com.testimages.www.imagetransform.utility.ImageHelper;

import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class ImageBlockRepository {
    public static final String ServiceUrl = String.format(BuildConfig.serviceUrl + "/%d/%d/", ImageInfo.TileWidth, ImageInfo.TileHeight);

    private final ImageInfo initImageInfo;
    private final Picasso picasso;

    public ImageBlockRepository(ImageInfo initImage, Picasso picasso) {
        this.initImageInfo = initImage;
        this.picasso = picasso;
    }

    public Observable<ImageBlock> getRowImageBlocks(int currentRow) {
        return getRowImageBlocks(currentRow,Schedulers.computation());
    }

    public Observable<ImageBlock> getRowImageBlocks(int currentRow, Scheduler scheduler) {
        final int totalColumnNumber = initImageInfo.getColumnNumber();
        Timber.i("TotalColumnNumber " + totalColumnNumber + " Row " + currentRow);

        return Observable.range(0, totalColumnNumber)
                .flatMap(columnNumber -> Observable.just(new ImageBlockInfo(currentRow, columnNumber))
                        .observeOn(scheduler)//Schedule in different thread
                        .map(imageBlockInfo -> getImageBlock(imageBlockInfo)));
    }

    @NonNull
    private ImageBlock getImageBlock(ImageBlockInfo imageBlockInfo) {

        Timber.i("Calculate in " + Thread.currentThread().getName());
        int initColor = ImageHelper.getBlockAverageColor(initImageInfo, imageBlockInfo);
        int red = Color.red(initColor);
        int green = Color.green(initColor);
        int blue = Color.blue(initColor);
        final String colorString = String.format("%02x%02x%02x", red, green, blue);
        final String colorTransferUrl = ServiceUrl + colorString;
        Timber.i("Color url " + colorTransferUrl);
        Bitmap bitmap = ImageHelper.loadTileFromService(picasso, colorTransferUrl);
        return new ImageBlock(imageBlockInfo, bitmap);
    }
}
