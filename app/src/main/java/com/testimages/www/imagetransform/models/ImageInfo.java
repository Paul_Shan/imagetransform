package com.testimages.www.imagetransform.models;

import android.graphics.Bitmap;

import com.google.common.base.Preconditions;
import com.testimages.www.imagetransform.BuildConfig;

public class ImageInfo {
    public final Bitmap bitmap;
    public static final int TileWidth = BuildConfig.tileWidth;
    public static final int TileHeight = BuildConfig.tileHeight;
    private int rowNumber = -1;
    private int columnNumber = -1;

    public ImageInfo(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isImageValid() {
        return bitmap != null && getHeight() > 0 && getWidth() > 0;
    }

    public int getRowNumber() {
        if (rowNumber < 0) {
            rowNumber = (int) Math.ceil(getHeight() / (double) TileHeight);
        }
        return rowNumber;
    }

    public int getPixelRowOffset(int rowNumber)
    {
        return rowNumber* TileHeight;
    }

    public int getPixelColOffset(int columnNumber)
    {
        return columnNumber* TileWidth;
    }

    public int getColumnNumber() {
        if (columnNumber < 0) {
            columnNumber = (int) Math.ceil(getWidth() / (double) TileWidth);
        }
        return columnNumber;
    }

    public int getWidth() {
        return bitmap.getWidth();
    }

    public int getHeight() {
        return bitmap.getHeight();
    }

    public int getValue(int x, int y) {
        Preconditions.checkElementIndex(x, getWidth());
        Preconditions.checkElementIndex(y, getHeight());
        return bitmap.getPixel(x, y);
    }


}
