package com.testimages.www.imagetransform.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.testimages.www.imagetransform.R;

//Only for test usage
public class TempImage {
    static Bitmap temp;

    public static void initImage(Context context) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        temp = BitmapFactory.decodeResource(context.getResources(), R.drawable.tile);
    }

    public static Bitmap getTempImage() {
        return temp;
    }
}
