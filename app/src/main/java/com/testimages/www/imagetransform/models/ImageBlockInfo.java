package com.testimages.www.imagetransform.models;

import com.google.common.base.MoreObjects;

public class ImageBlockInfo {

   final private int rowNumber;
   final private int columnNumber;

    public ImageBlockInfo(int rowNumber,int columnNumber) {
        this.columnNumber = columnNumber;
        this.rowNumber = rowNumber;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Rowumn", rowNumber)
                .add("Column ",columnNumber)
                .toString();
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    public int getRowNumber() {
        return rowNumber;
    }
}
